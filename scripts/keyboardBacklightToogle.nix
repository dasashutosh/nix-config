{ pkgs, config, recall }:

with pkgs;
let
  boolToStr = value: if value then "true" else "false";
in
writeShellScriptBin "keyboardBacklightToogle" ''
getBrightness()
{
  ${pkgs.dbus_tools}/bin/dbus-send                                  \
      --type=method_call --print-reply=literal --system             \
      --dest="org.freedesktop.UPower"                               \
      "/org/freedesktop/UPower/KbdBacklight"                        \
      "org.freedesktop.UPower.KbdBacklight.GetBrightness"           \
      | awk '{print $2}'
}
getMaxBrightness()
{
  ${pkgs.dbus_tools}/bin/dbus-send                                  \
      --type=method_call --print-reply=literal --system             \
      --dest="org.freedesktop.UPower"                               \
      "/org/freedesktop/UPower/KbdBacklight"                        \
      "org.freedesktop.UPower.KbdBacklight.GetMaxBrightness"        \
      | awk '{print $2}'
}
setBrightness()
{
	${pkgs.dbus_tools}/bin/dbus-send --system --type=method_call  --dest="org.freedesktop.UPower" "/org/freedesktop/UPower/KbdBacklight" "org.freedesktop.UPower.KbdBacklight.SetBrightness" int32:"$1"
}

current=$( getBrightness )
max=$( getMaxBrightness )
if ${boolToStr recall} ;
then
  setBrightness "$current";
else
  if test $current -lt $max ;
  then
    value=$(( $current + 1 ))
    setBrightness "$value"
  else
    setBrightness "0";
  fi
fi

''
