{ pkgs }:

let
  ghcidOrig = pkgs.vimUtils.buildVimPlugin {
    pname = "ghcid";
    version = "692df0f9a712c92f7ed48993693bfca1e5d87288";
    src = pkgs.fetchFromGitHub {
      owner = "ndmitchell";
      repo = "ghcid";
      rev = "692df0f9a712c92f7ed48993693bfca1e5d87288";
      sha256 = "0661i147grqin8pgb5bv88y85dxwcn0fkzrm190d0f9q479hwrnj";
    };
  };
in {
  ghcid = ghcidOrig // { rtp = "${ghcidOrig.rtp}/plugins/nvim"; };
  tmuxline = pkgs.vimUtils.buildVimPlugin {
    pname = "tmuxline";
    version = "1142333aaef6b542f46ccd5c1e806bdd9382f005";
    src = pkgs.fetchFromGitHub {
      owner = "edkolev";
      repo = "tmuxline.vim";
      rev = "1142333aaef6b542f46ccd5c1e806bdd9382f005";
      sha256 = "1dr2ndixvnx6ir92kgij66hn29nj0aadbz643ayzf5vra66xichn";
    };
  };
  hsimport = pkgs.vimUtils.buildVimPlugin {
    pname = "vim-hsimport";
    version = "v0.4.4";
    src = pkgs.fetchFromGitHub {
      owner = "dan-t";
      repo = "vim-hsimport";
      rev = "v0.4.4";
      sha256 = "0ml0vy0zmk8p7sjh6mddlhqiwz568yjjhx3n6sb0gz5zgscgwvnw";
    };
  };
  unite-haskellimport = pkgs.vimUtils.buildVimPlugin {
    pname = "unite-haskellimport";
    version = "440d605";
    src = pkgs.fetchFromGitHub {
      owner = "ujihisa";
      repo = "unite-haskellimport";
      rev = "440d6051fb71f7cc5d4b4c83f02377bfa48d0acf";
      sha256 = "1qqbb7prm3hyjhzb5hglnyak35gr4dkj34wcklcjg9zxg9qm1mq0";
    };
  };
  indentLine = pkgs.vimUtils.buildVimPlugin {
    pname = "indentLine";
    version = "v2.0";
    src = pkgs.fetchFromGitHub {
      owner = "Yggdroot";
      repo = "indentLine";
      rev = "v2.0";
      sha256 = "0pbd05l25r95zd7721dvgys22k35aqh43l8w2z55g29kn32c4vrm";
    };
  };
  delimitMate = pkgs.vimUtils.buildVimPlugin {
    pname = "delimitMate";
    version = "v2.7";
    src = pkgs.fetchFromGitHub {
      owner = "Raimondi";
      repo = "delimitMate";
      rev = "2.7";
      sha256 = "0p7w21hja1a87mic5z488q6bzafx89h54faj7zznb8bfam8h4qpc";
    };
  };
  ultisnips = pkgs.vimUtils.buildVimPlugin {
    pname = "ultisnips";
    version = "v3.1";
    src = pkgs.fetchFromGitHub {
      owner = "SirVer";
      repo = "ultisnips";
      rev = "3.1";
      sha256 = "0p9d91h9pm0nx0d77lqsgv6158q052cyj4nm1rd6zvbay9bkkf8b";
    };
  };
  vim-javascript-syntax = pkgs.vimUtils.buildVimPlugin {
    pname = "vim-javascript-syntax";
    version = "v0.8.2";
    src = pkgs.fetchFromGitHub {
      owner = "jelera";
      repo = "vim-javascript-syntax";
      rev = "0.8.2";
      sha256 = "04wbhsg6ybb3d1lxgidgqb16hbggsjj864gbd3fxp71vcja444qa";
    };
  };
}
