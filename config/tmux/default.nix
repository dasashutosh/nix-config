{ pkgs }:

with pkgs;
let
  tmuxTheme = builtins.fetchurl {
      url = https://raw.githubusercontent.com/jimeh/tmux-themepack/master/powerline/block/cyan.tmuxtheme;
      sha256 = "0cdpkp6cf3v24x7wyhxpzr7jxfwmb3m8lwppjjdaz05xdsprfm9g";
  };
  tmuxConf = builtins.readFile ./tmux.conf + ''
# Theme
# source /usr/lib/python3.7/site-packages/powerline/bindings/tmux/powerline.conf
# source-file "/home/ashutosh/tmp/tmux-themepack/powerline/block/cyan.tmuxtheme"
source-file ${tmuxTheme}
  '';
in {
  enable = true;
  terminal = "screen-256color";
  baseIndex = 1;
  shortcut = "Space";
  escapeTime = 0;
  historyLimit = 1000000;
  keyMode = "vi";
  reverseSplit = true;
  extraConfig = tmuxConf;
}
