{ pkgs, rofiElectronAppsRunner, shareLink, onAttachMonitor, polybarLaunch }:

with pkgs;
let
  #workspaces = ["λ" "" "🗩" "" "♫" "⚙" "" "🗎" ""];
  workspaces = ["code" "web" "messenger" "admin" "docs" "search" "scratch" "scratch" "scratch"];
  numbers = map toString (lib.range 1 9);
  workspaceNumbers = lib.zipListsWith (x: y: x + ": " + y) numbers workspaces ;
  useWithModifier = mod: lib.mapAttrs' (k: v: lib.nameValuePair (mod + "+" + k) v);
  changeToi3ExitOption = lib.mapAttrs (k: v: "exec --no-startup-id i3exit ${v}, mode \"default\"");
  powerManagement = writeScript "rofi-power-management" ''
    #!${rofi-menugen}/bin/rofi-menugen

    #begin main
    prompt="Select:"
    add_exec 'Lock'         'exec i3exit lock'
    add_exec 'Logout'       'exec i3exit logout'
    add_exec 'Sleep'        'exec i3exit suspend'
    add_exec 'Hibernate'    'exec i3exit hibernate'
    add_exec 'Reboot'       'exec i3exit reboot'
    add_exec 'PowerOff'     'systemctl poweroff'
    #end main
  '';
in
{
  config = rec {
    fonts = [
      "Noto Sans Symbols 10"
      "Noto Sans Symbols2 10"
      "Noto Sans Mono 10"
      "SauceCodePro Nerd Font 10"
    ];
    assigns = {
      "number \"${lib.elemAt workspaceNumbers 4}\"" = [{ class = "qutebrowser"; }];
      "number \"${lib.elemAt workspaceNumbers 1}\"" = [{ class = "Vivaldi-stable"; }];
      "number \"${lib.elemAt workspaceNumbers 5}\"" = [{ class = "Surf"; }];
    };
    modifier = "Mod4";
    keybindings = useWithModifier modifier ({
      "Return" = "exec ${termite}/bin/termite";
      "Shift+Return" = "exec ${rxvt_unicode-with-plugins}/bin/urxvt";
# kill focused window
      "q" = "kill";
      "h" = "focus left";
      "j" = "focus down";
      "k" = "focus up";
      "l" = "focus right";
      #"g" = ''exec ${wmfocus}/bin/wmfocus --fill --bgcolor "rgba(30, 30, 30, 0.5)"  -c asdf --textcolor red'';
      "t" = "split toggle";
      "f" = "fullscreen toggle";
      "s" = "layout stacking";
      "w" = "layout tabbed";
      "e" = "layout toggle stacking tabbed splith splitv";
# change focus between tiling / floating windows
      "space" = "focus mode_toggle";
      "Shift+space" = "floating toggle";
      "a" = "focus child";
      "Shift+a" = "focus parent";
# reload the configuration file
      "c" = "reload";
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
      "Shift+c" = "restart";
      "r" = "mode resize";
      "BackSpace" = "mode $leave";
      "Shift+BackSpace" = "exec --no-startup-id ${powerManagement}";
      "minus" = "split vertical";
      "bar" = "split horizontal";
      "Shift+h" = "move left";
      "Shift+j" = "move down";
      "Shift+k" = "move up";
      "Shift+l" = "move right";
      "grave" = "workspace back_and_forth";
      #"asciitilde" = "move container to workspace back_and_forth";
      "d" = "exec rofi -show drun";
      "Shift+d" = "exec ${rofiElectronAppsRunner}/bin/rofiElectronAppsRunner";
      "slash" = ''exec quickswitch.py -d "rofi -i -dmenu"'';
      "Shift+slash" = ''exec menu-surfraw'';
      "v" = "exec ${shareLink}/bin/shareLink";
      "Tab" = "workspace next";
      "Shift+Tab" = "workspace prev";
    } //
    lib.foldl (x: y: x // y) {}
      (lib.zipListsWith
        (i: n: {
          "${i}" = "workspace number ${n}";
          "Shift+${i}" = "move container to workspace number ${n}";
        })
        numbers
        workspaceNumbers)
    );
    keycodebindings = useWithModifier modifier {
    };
    modes = {
      "$leave" = changeToi3ExitOption {
        l = "lock";
        e = "logout";
        s = "suspend";
        h = "hibernate";
        r = "reboot"; } // {
        Return = "mode default";
        Escape = "mode default";
      };
      resize = {
        h = "resize shrink width 3 px or 3 ppt";
        j = "resize shrink height 3 px or 3 ppt";
        k = "resize grow height 3 px or 3 ppt";
        l = "resize grow width 3 px or 3 ppt";
        Return = "mode default";
        Escape = "mode default";
      };
    };
    bars = [];
    startup = [
      {
        command = "systemctl --user restart polybar";
        always = true;
        notification = false;
      }
      {
        command = "nm-applet";
        always = true;
        notification = false;
      }
      # {
      #   command = ''
      #     i3-msg 'workspace number 1; exec --no-startup-id urxvt -e bash -c "neofetch | lolcat && zsh"'
      #   '';
      #   always = false;
      #   notification = false;
      # }
      # {
      #   command = ''
      #     i3-msg 'workspace number 2; exec --no-startup-id google-chrome-stable'
      #   '';
      #   always = false;
      #   notification = false;
      # }
    ];
    colors = {
      focused = {
        border = "#586e75";
        background = "#586e75";
        indicator = "#fdf6e3";
        childBorder = "#268bd2";
        text = "";
      };
      focusedInactive = {
        border = "#073642";
        background = "#073642";
        indicator = "#93a1a1";
        childBorder = "#002b36";
        text = "";
      };
      unfocused = {
        border = "#002b36";
        background = "#002b36";
        indicator = "#586e75";
        childBorder = "#002b36";
        text = "";
      };
      urgent = {
        border = "#dc322f";
        background = "#dc322f";
        indicator = "#fdf6e3";
        childBorder = "#dc322f";
        text = "";
      };
    };
    focus = {
      followMouse = false;
      forceWrapping = true;
      mouseWarping = false;
    };
    window = {
      border = 0;
      hideEdgeBorders = "both";
      commands = [
        { criteria = { class = "Electron"; title = ".*Slack.*"; }; command = "move container to workspace ${lib.elemAt workspaceNumbers 2}";}
        { criteria = { class = "Electron"; title = ".*WhatsApp.*"; }; command = "move container to workspace ${lib.elemAt workspaceNumbers 2}";}
        { criteria = { class = "Electron"; title = ".*Gitter.*"; }; command = "move container to workspace ${lib.elemAt workspaceNumbers 2}";}
        { criteria = { class = "Electron"; title = ".*Hangouts.*"; }; command = "move container to workspace ${lib.elemAt workspaceNumbers 2}";}
      ];
    };
    floating = {
      border = 0;
      criteria = [
        { class = "Electron"; }
        { class = "Pavucontrol"; }
        { class = "Skype"; }
        { class = "pinentry"; }
        { window_role = "pop-up"; }
        { window_role = "task_dialog"; }
        { title = "Preferences"; }
      ];
    };
  };
  enable = true;
  package = i3-gaps;
  extraConfig = ''
set $mod Mod4

popup_during_fullscreen smart

# floating_maximum_size 480 x 640
# floating_minimum_size 480 x 640
#
###########For Multimedia Keys####################
# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute  @DEFAULT_SINK@ toggle # mute sound

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec --no-startup-id light -A 5
bindsym XF86MonBrightnessDown exec --no-startup-id light -U 5

# monitor
bindsym XF86Display exec --no-startup-id ${onAttachMonitor}/bin/onAttachMonitor
bindsym $mod+XF86Display exec --no-startup-id ${polybarLaunch}/bin/polybarLaunch

# Touchpad controls
#bindsym XF86TouchpadToggle exec /some/path/toggletouchpad.sh # toggle touchpad

# Media player controls
bindsym XF86AudioPlay exec mpc toggle
bindsym $mod+XF86AudioNext exec mpc seek +00:00:30
bindsym $mod+XF86AudioPrev exec mpc seek -00:00:30
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev
bindsym $mod+m exec quickswitch.py -m -d "rofi -i -dmenu"
bindsym $mod+Shift+m exec quickswitch.py -m -f -d "rofi -i -dmenu"
bindsym $mod+p exec passdo --copy
bindsym $mod+Shift+p exec passdo --type
bindsym $mod+n exec passdo --notify
################################################################

#::::::::::::::::::: Settings for i3-gaps :::::::::::::::::::::#
################################################################
# Set inner/outer gaps
#gaps inner 8
#gaps outer -4
set $default_gaps_inner 5
set $default_gaps_outer 5
#set $default_gaps_inner 8
#set $default_gaps_outer -4
gaps inner $default_gaps_inner
gaps outer $default_gaps_outer
for_window [class="^.*"] border pixel 2
#for_window [class="^.*"] border normal 0
bindsym $mod+z		gaps outer current plus 5
bindsym $mod+Shift+z	gaps outer current minus 5


# Additionally, you can issue commands with the following syntax. This is useful to bind keys to changing the gap size.
# gaps inner|outer current|all set|plus|minus <px>
# gaps inner all set 10
# gaps outer all plus 5

# Smart gaps (gaps used if only more than one container on the workspace)
smart_gaps on

# Smart borders (draw borders around container only if it is not the only container on this workspace)
# on|no_gaps (on=always activate and no_gaps=only activate if the gap size to the edge of the screen is 0)
smart_borders on

# Press $mod+Shift+g to enter the gap mode. Choose o or i for modifying outer/inner gaps. Press one of + / - (in-/decrement for current workspace) or 0 (remove gaps for current workspace). If you also press Shift with these keys, the change will be global for all workspaces.
set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
bindsym $mod+Shift+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+Shift+t	gaps inner all set 15; gaps outer all set 15

#bindsym $mod+Shift+d exec rofi -show window
bindsym $mod+bracketright move workspace to output right
bindsym $mod+bracketleft move workspace to output right
bindsym $mod+braceright move workspace to output up
bindsym $mod+braceleft move workspace to output down

# Make the currently focused window a scratchpad
#bindsym $mod+Shift+minus move scratchpad

# Show the first scratchpad window
#bindsym $mod+minus scratchpad show

show_marks yes
    '';
}
