{

  #"URxvt*font" = "xft:FuraCode\\ Nerd\\ Font:antialias=true:hinting=true:style=Regular:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  #"URxvt*boldFont" = "xft:FuraCode\\ Nerd\\ Font:antialias=true:hinting=true:style=Bold:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  #"URxvt*italicFont" = "xft:FuraCode\\ Nerd\\ Font:antialias=true:hinting=true:style=Medium\\ Italic:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  #"URxvt*boldItalicFont" = "xft:FuraCode\\ Nerd\\ Font:antialias=true:hinting=true:style=Bold\\ Italic:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  "URxvt*font" = "xft:SauceCodePro\\ Nerd\\ Font\\ Mono:antialias=true:hinting=true:style=Regular:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  "URxvt*boldFont" = "xft:SauceCodePro\\ Nerd\\ Font\\ Mono:antialias=true:hinting=true:style=Bold:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  "URxvt*italicFont" = "xft:SauceCodePro\\ Nerd\\ Font\\ Mono:antialias=true:hinting=true:style=Medium\\ Italic:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  "URxvt*boldItalicFont" = "xft:SauceCodePro\\ Nerd\\ Font\\ Mono:antialias=true:hinting=true:style=Bold\\ Italic:size=12:minspace=False, xft:DejaVu Sans:pixelsize=12";
  "rofi.font" = "Hack Nerd Font Mono 11";
  #"URxvt*font" = "xft:Source Code Pro for Powerline:size=11";
  #"URxvt*boldFont" = "xft:Source Code Pro for Powerline";
  "URxvt.color12" = "rgb:5c/5c/ff";

# Fix the annoying gaps between powerline symbols
  "URxvt.letterSpace" = -1;

# Larger History limit
  "URxvt.saveLines" = 1000000;

  "Xft.antialias" = true;
  "Xft.autohint" = false;
  # "Xft.dpi" = 96;
  "Xft.dpi" = 120;
  "Xft.hinting" = true;
  "Xft.hintstyle" = "hintslight";
  "Xft.lcdfilter" = "lcddefault";
  "Xft.rgba" = "rgb";


# Disable scrollbar
  "URxvt.scrollBar" = false;


# Enables copy/paste with Ctrl-Shift-C/V
  "URxvt.iso14755" = false;
  "URxvt.iso14755_52" = false;
  "URxvt.perl-ext-common" = "default,clipboard,matcher";
  "URxvt.keysym.Shift-Control-C" = "perl:clipboard:copy";
  "URxvt.keysym.Shift-Control-V" = "perl:clipboard:paste";

# Make URLs clickable with left mouse button;
  "URxvt.url-launcher" = "/usr/bin/xdg-open";
  "URxvt.matcher.button" = 1;


  "urxvt*transparent" = true;
  "urxvt*shading" = 10;
  # "urxvt*tintColor" = "#232c33";


  "URxvt*skipBuiltinGlyphs" = true;



# Colors {{{

# special
  "*foreground" = "#f0f0f0";
  "*background" = "#232c33";
  "*cursorColor" = "#f0f0f0";


# black
# Second black is grey
  "*color0" = "#232c33";
  "*color8" = "#70838c";

# red
  "*color1" = "#99736e";
  "*color9" = "#99736e";

# green
  "*color2" = "#78a090";
  "*color10" = "#78a090";

# yellow
  "*color3" = "#bfb7a1";
  "*color11" = "#bfb7a1";

# blue
  "*color4" = "#7c9fa6";
  "*color12" = "#7c9fa6";

# magenta
  "*color5" = "#BF9C86";
  "*color13" = "#BF9C86";

# cyan
  "*color6" = "#99BFBA";
  "*color14" = "#99BFBA";

# white
  "*color7" = "#f0f0f0";
  "*color15" = "#f0f0f0";




  #"URxvt*termName"  = "rxvt-unicode-256color";
  "URxvt.depth" = 32;
  "URxvt.colorUL" = "#4682B4";
  #"URxvt.tabbed.saveLines" = 10000;
  "URxvt.secondaryScroll" = true;

  #"URxvt.perl-ext-common" = "default,matcher,tabbedex,keyboard-select,autocomplete-ALL-the-things,selection-to-clipbard";
  #"URxvt.perl-ext-blacklist" = "tabs";


  "URxvt.cursorBlink" = true;
  #"URxvt.cursorUnderline" = true;

  #manage font size;
  "URxvt.keysym.C-S-plus" = "font-size:increase";
  "URxvt.keysym.C-KP_Add" = "font-size:increase";
  "URxvt.keysym.C-underscore" = "font-size:decrease";
  "URxvt.keysym.C-KP_Subtract" = "font-size:decrease";
}
