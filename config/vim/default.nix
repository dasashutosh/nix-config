{pkgs}:

let
  plugins = pkgs.vimPlugins // pkgs.callPackage ../../packages/vim-custom-plugins.nix { inherit pkgs; };
in
{
  enable = true;
  viAlias = true;
  vimAlias = true;
  withNodeJs = true;
  withPython = true;
  withPython3 = true;
  configure = with plugins; {
    customRC = pkgs.lib.readFile ./vimrc;
    plug.plugins = [
      (ale.overrideAttrs(oldAttrs: { patches = ./cabal-ghc.patch; }))
      ack-vim
      airline
      calendar
      calendar-vim
      CSApprox
      colors-solarized
      commentary
      denite-nvim
      # delimitMate
      deoplete-nvim
      fugitive
      gitgutter
      ##ghcmod-vim
      ghcid
      haskell-vim
      Hoogle
      hsimport
      indentLine
      LanguageClient-neovim
      ##intero-neovim
      molokai
      ##neco-ghc
      #neoformat
      #neomake
      neomru
      nerdcommenter
      nerdtree
      polyglot
      psc-ide-vim
      purescript-vim
      supertab
      #syntastic
      tmuxline
      unite
      unite-haskellimport
      ultisnips
      vim-airline-themes
      vim-better-whitespace
      vim-devicons
      vim-eunuch
      vim-hardtime
      vim-hdevtools
      vim-javascript-syntax
      vim-nix
      ##vim-searchhi
      vim-slime
      vim-trailing-whitespace
      ##vim-tree
    ] ++ [
      rust-vim
      vim-racer
    ];
  };
}
